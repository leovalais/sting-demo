(defpackage :demo-tests
  (:use :cl :demo :sting))

(in-package :demo-tests)

(define-test add-one-thing
  (add :apple)
  (assert-equal '(:apple 1) *stock*))

(define-test add-many-things
  (add :apple)
  (add :melon)
  (assert-equal '(:melon 1 :apple 1) *stock*))

(define-before ()
  (setf *stock* '()))

(define-test use-one
  (add :apple)
  (use :apple)
  (assert-not (has? :apple)))

(define-test not-available
  (assert-error error (use :pear)))
