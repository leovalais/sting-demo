(defpackage :demo
  (:use :cl)
  (:export *stock*
           add
           use
           has?))

(in-package :demo)

(defvar *stock* '())

(defun add (thing)
  (setf (getf *stock* thing)
        (1+ (or (getf *stock* thing)
                0))))

(defun has? (thing)
  (let ((c (getf *stock* thing)))
    (and c (> c 0))))

(defun use (thing)
  (if (has? thing)
      (decf (getf *stock* thing))
      (error "~S not available" thing)))
