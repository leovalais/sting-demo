(asdf:defsystem :demo
  :name "demo"
  :version "0.1"
  :maintainer "Léo Valais"
  :author "Léo Valais"
  :licence "MIT"
  :depends-on ()

  :serial t
  :components
  ((:file "demo")))

(asdf:defsystem :demo/tests
  :name "demo/tests"
  :version "0.1"
  :maintainer "Léo Valais"
  :author "Léo Valais"
  :licence "MIT"
  :depends-on (:sting :demo)

  :serial t
  :components
  ((:file "tests")))
